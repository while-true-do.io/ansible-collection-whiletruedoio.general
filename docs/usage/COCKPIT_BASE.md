# whiletruedoio.general.cockpit_base

A basic setup that can be customized, featuring the Cockpit Web interface.

## Description

[Cockpit](https://cockpit-project.org/) is a web interface for Linux management.
It can be used to review logs, update a machine or configure the network and
much more. With this playbook, you will get a basic Linux server, which powers
Cockpit for easier management and convenience. In addition, there is also some
very basic security integrated.

It is mostly intended for private networks, like in companies or at home.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
