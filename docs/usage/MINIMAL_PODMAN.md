# whiletruedoio.general.minimal_podman

A minimal Podman server, which can be used for container workloads.

## Description

The minimal Podman server is perfect for becoming your small container host.
You can run containers easily from the command line or with remote tools or
automation.

It is mostly intended for private networks, like in companies or at home.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
