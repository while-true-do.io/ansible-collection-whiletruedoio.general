# whiletruedoio.general.minimal_kvm

A minimal KVM hypervisor, which can be used for virtual machine workloads.

## Description

The minimal KVM hypervisor can be used to extend your existing virtualization
environment or to build one from scratch. It powers KVM, including nested
virtualization and can be controlled on the command line.

It is mostly intended for private networks, like in companies or at home.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
