# whiletruedoio.general.minimal_base

A minimal, basic setup that can be customized to your liking.

## Description

The minimal base server comes almost as minimal as possible. There are some
minor security additions, but otherwise, you have to put your customizations
on top.

It is mostly intended for private networks, like in companies or at home.

## Dependencies

None.

## Conflicts

None.

## Variables

TBD

## Used roles

TBD

## Additional hints

TBD

## Example

TBD
