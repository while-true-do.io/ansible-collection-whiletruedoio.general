# ToDo

This small list includes some ToDo items, I might tackle soon'ish.

## Mandatory

- [x] mkdocs documentation
- [x] make mkdocs part of the pipeline
- [x] roles must be documented within role directories to be present in galaxy
  - [x] move from docs
  - [x] replace relative links with static links to the repository
- [x] galaxy docs
  - [x] replace relative links with absolutes in the README.md
  - [x] maintain meta/main.yml for all roles
- [ ] refine roles
  - [ ] all template tasks should have some kind of validation
  - [ ] improve examples in roles docs
  - [ ] all tasks should support dry-run/check mode
  - [ ] validate functionality of each role

## Optional

- [ ] TuneD should support profiles
- [ ] find a cool way for the reboot handlers
- [ ] automatic system updates?
- [ ] mkdocs versioning with mike
      <https://github.com/squidfunk/mkdocs-material-example-versioning>
- [x] mkdocs material themes
- [ ] mkdocs analytics with plausible
      <https://pypi.org/project/material-plausible-plugin/>
- [ ] move to pymarkdown or alternative to avoid npm

## More roles

- [x] gitlab runner
- [ ] docker
- [x] avahi
- [ ] k3s
  - [ ] initial deployment (on Alma/Rocky/Rhel)
  - [ ] hardening
  - [ ] storage (longhorn)
  - [ ] k3s feature selection
  - [ ] dashboard
  - [ ] node removal
  - [ ] node update
  - [ ] node addition
- [x] auditd
- [ ] aide
- [x] sudo
- [x] kdump
- [x] usbguard
- [x] tmux autostart
- [ ] network
- [ ] NFS
- [ ] pxe
- [ ] dhcp
- [ ] keepalived
- [ ] fapolicyd

## More playbooks

- [ ] hardened playbooks
- [ ] iot device
- [ ] kubernetes cluster (in scope?)
