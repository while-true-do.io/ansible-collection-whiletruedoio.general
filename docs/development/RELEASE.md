---
tags:
  - "development"
---

# Release

For releases, I use [Semantic Versioning](https://semver.org/). Therefore, it
is strongly recommended to use proper
[Conventional Commits](https://www.conventionalcommits.org/) for your merge
requests.

A release is triggered as soon as a tag is pushed to `main` branch. This can
only be done from the code maintainers. The general process looks like this:

```shell
# Pull latest updates
$ git checkout main
$ git pull

# Check for existing commits
$ git tag

# Check for latest changes
$ git log

# Tag the new version
$ git tag x.y.z

# Push the tags
$ git push --tags
```

Aside these manual steps, everything is automated via
[GitLab-CI](https://docs.gitlab.com/ee/ci/). You can read the entire pipeline in
the `.gitlab-ci.yml` file.

Afterwards the usual test pipeline will run and release artifacts will be
created. If everything is fine, a new release will be available and public.
Additionally, we will send out notifications to our contact channels.
