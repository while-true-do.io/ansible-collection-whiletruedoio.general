# whiletruedoio.general.firewalld

An Ansible Role to install and configure firewalld.

## Description

The role installs and configures firewalld and ensures that the service is
running and properly configured.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.firewalld"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.firewalld"
      vars:
        firewalld_package_state: "latest"
```
