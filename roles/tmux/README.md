# whiletruedoio.general.tmux

An Ansible Role to install and configure tmux.

## Description

TMUX is pretty useful when it comes to multi-tasking and remote management.
Instead of dropping your entire terminal when losing the ssh connection, TMUX
will keep it open for you. This role takes care of installing and configuring
TMUX.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.tmux"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.tmux"
      vars:
        tmux_package_state: "latest"
```
