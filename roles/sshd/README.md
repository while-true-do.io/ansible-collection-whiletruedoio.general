# whiletruedoio.general.sshd

An Ansible Role to install and configure sshd/openssh-server.

## Description

The role takes care of the installation of a standard openssh server and its
configuration. There are some minor security enhancements enabled, too.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.sshd"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.sshd"
      vars:
        sshd_package_state: "latest"
        sshd_config_PermitRootLogin: "no"
        sshd_config_Banner: "/etc/ssh/sshd_config.d/banner"
```
