# whiletruedoio.general.certbot

An Ansible Role to install and configure certbot.

## Description

Having a proper certbot environment is mandatory for most workflows that need to
be done via SSH or console input.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.certbot"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.certbot"
      vars:
        certbot_package_state: "latest"
```
