# whiletruedoio.general.hostname

An Ansible Role to configure the hostname.

## Description

The role configures the hostname, either from the given inventory hostname or
a given hostname by a variable.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.hostname"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.hostname"
      vars:
        hostname_name: "special.host.name"
```
