# whiletruedoio.general.timesyncd

An Ansible Role to install and configure systemd-timesyncd.

## Description

Timesyncd is a lightweight NTP client and can be used as a replacement for
Chrony or NTPD on most machines. This role takes care of the proper installation
and configuration.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.timesyncd"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.timesyncd"
      vars:
        timesyncd_package_state: "latest"
```
