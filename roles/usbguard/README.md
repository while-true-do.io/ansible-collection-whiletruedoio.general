# whiletruedoio.general.usbguard

An Ansible Role to install and configure usbguard.

## Description

A service like usbguard can protect a system from malicious USB devices. One
can allow a specific type of devices or exact device IDs.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.usbguard"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.usbguard"
      vars:
        usbguard_package_state: "latest"
```
