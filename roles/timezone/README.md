# whiletruedoio.general.timezone

An Ansible Role to configure timezone.

## Description

A timezone should be configured thoughtfully and with intent. On servers, you
might default to UTC (avoiding DST and log time issues), but you can also
configure your desired timezone.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.timezone"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.timezone"
      vars:
        timezone_name: "Asia/Tokyo"
```
