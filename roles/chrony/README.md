# whiletruedoio.general.chrony

An Ansible Role to install and configure chrony.

## Description

Chrony is a tool that can be used as a NTP client or NTP server.

## Dependencies

None.

## Conflicts

The role itself does not, but the Chrony service conflicts with other NTP
services.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.chrony"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.chrony"
      vars:
        chrony_package_state: "latest"
```
