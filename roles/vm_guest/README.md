# whiletruedoio.general.vm_guest

An Ansible Role to install VM guest tools.

## Description

VM Guests often need special services to be aware of the underlying hypervisor
or receive commands from them. If you use your Linux in a virtual environment,
this role might help you to communicate with VM Ware or KVM/QEMU.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.vm_guest"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.vm_guest"
      vars:
        vm_guest_package_state: "latest"
```
