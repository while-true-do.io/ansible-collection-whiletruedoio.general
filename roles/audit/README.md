# whiletruedoio.general.audit

An Ansible Role to install and configure audit.

## Description

audit is useful to audit activities on a machine and report these activities.
The role will provide a basic installation, including grub updates. Rules need
to be provided on top.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.audit"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.audit"
      vars:
        audit_package_state: "latest"
```
