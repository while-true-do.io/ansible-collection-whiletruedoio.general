# whiletruedoio.general.user

An Ansible Role to configure users on a system.

## Description

This role configures users, including their public key, which can come in
handy, if you don't have a central authentication mechanism.

## Dependencies

None.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

If you want to use this role and provide passwords, you have to create valid
password hashes beforehand. This can be done with one of the following commands:

With Ansible:

```shell
ansible all -i localhost, -m debug -a "msg={{ 'mypassword' | password_hash('sha512', 'mysecretsalt') }}"
```

With mkpasswd:

```shell
$ mkpasswd --method=sha-512
Password:
$6$2Y0zoyxnp5NMdF0T$4jweZk15WtT1Y.CYswo0GLs.Mb9ah2xWEWDFVQEIQEtZggbIo7hY0PX5rbdJ7CNseuc69D203W6WbbPRMtEln0
```

With Python:

```shell
python -c "from passlib.hash import sha512_crypt; import getpass; print(sha512_crypt.using(rounds=5000).hash(getpass.getpass()))"
```

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.user"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.user"
      vars:
        user_users:
          - username: "alice"
          - username: "bob"
            password_hash: "$6$............"
          - username: "chad"
            state: "absent"
```
