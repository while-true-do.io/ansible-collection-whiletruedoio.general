# whiletruedoio.general.gitlab_runner

An Ansible Role to install and configure gitlab_runner.

## Description

This role installs and configures a gitlab-runner (Linux) on the given host.
This might be handy for self-hosting your runners.

## Dependencies

There are no baked-in dependencies (for now?), but you need to have a working
Podman environment, as provided by the whiletruedoio.general.podman role.

## Conflicts

None.

## Variables

The role comes with default variables (via `defaults/main.yml`), which can be
overridden.

All variables will be tested against the provided `meta/argument_specs.yml`
automatically.

## Additional hints

None.

## Examples

The below examples should help to explain how the role can be used.

```yaml
# Simple example with defaults

- name: "Examples for the usage"
  hosts: "yourhosts"

  tasks:

    - name: "Example with defaults"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.gitlab_runner"

    - name: "Example with customization"
      ansible.builtin.import_role:
        name: "whiletruedoio.general.gitlab_runner"
      vars:
        gitlab_runner_package_state: "latest"
```
